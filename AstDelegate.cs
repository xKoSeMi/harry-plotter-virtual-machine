﻿using System.Collections.Generic;

namespace HarryPlotterVirtualMachine
{
    
    /// <summary>
    /// Klasa implementująca metody sprawdzające warunek zakończenia parsowania.
    /// </summary>
    public class AstDelegate
    {
        
        /// <summary>
        /// Delegat oznaczający warunek zakończenia parsowania.
        /// </summary>
        /// <param name="l">leksem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">index w liście</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endChar">znak zakończenia</param>
        /// <returns>wartość logiczna</returns>
        public delegate bool EndParseCondition(Lexem l, IReadOnlyList<Lexem> list, int i, int lineNumber, char endChar);

        /// <summary>
        /// Sprawdzanie według numeru linii.
        /// </summary>
        /// <param name="l">leksem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">index w liście</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endChar">znak zakończenia</param>
        /// <returns>wartość logiczna</returns>
        public static bool CheckByLine(Lexem l, IReadOnlyList<Lexem> list, int i,
            int lineNumber, char endChar)
        {
            return l.LineNumber > lineNumber;
        }

        /// <summary>
        /// Sprawdzanie według znaku.
        /// </summary>
        /// <param name="l">leksem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">index w liście</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endChar">znak zakończenia</param>
        /// <returns>wartość logiczna</returns>
        public static bool CheckByChar(Lexem l, IReadOnlyList<Lexem> list, int i,
            int lineNumber, char endChar)
        {
            return l.Text[0] == endChar;
        }

        /// <summary>
        /// Sprawdzanie według numeru linii lub znaku w wyrażeniu logicznym.
        /// </summary>
        /// <param name="l">leksem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">index w liście</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endChar">znak zakończenia</param>
        /// <returns>wartość logiczna</returns>
        public static bool CheckByLineCharBoolean(Lexem l, IReadOnlyList<Lexem> list, int i,
            int lineNumber, char endChar)
        {
            var bracketCheck = i < list.Count - 1 && list[i + 1].Text[0] == '}';
            var charCheck = l.Text[0] == endChar;
            var lineCheck = l.LineNumber > lineNumber;
            var booleanCheck = Ast.IsComparingOperator(l);
            return charCheck || bracketCheck || lineCheck || booleanCheck;
        }

        /// <summary>
        /// Sprawdzanie w wyrażeniu matematycznym.
        /// </summary>
        /// <param name="l">leksem</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="i">index w liście</param>
        /// <param name="lineNumber">numer linii</param>
        /// <param name="endChar">znak zakończenia</param>
        /// <returns>wartość logiczna</returns>
        public static bool CheckMath(Lexem l, IReadOnlyList<Lexem> list, int i,
            int lineNumber, char endChar)
        {
            return (endChar > 0 && l.Text[0] == endChar)
                   || (l.LineNumber > lineNumber)
                   || Ast.IsComparingOperator(l);
        }
    }
}