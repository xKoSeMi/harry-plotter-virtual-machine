﻿using System;
using System.Collections.Generic;
using System.Linq;
 
namespace HarryPlotterVirtualMachine
{
    ///<summary>
    /// Klasa odpowiedzialna za podział kodu na leksemy
    /// </summary>
    public class Lexem
    {
        public string Text { get; private set; }

        private int _lineNumber;

        /// <summary>
        /// Numer linii w kodzie programu.
        /// </summary>
        public int LineNumber
        {
            get => _lineNumber;
            set => _lineNumber = value + 1;
        }

        /// <summary>
        /// Metoda do testowania podziału na lexemy.
        /// </summary>
        public static void LexemTest()
        {
            var lines = System.IO.File.ReadAllLines("/home/bolek/Code.hpvm");
            var list = ParseCode(lines);
            var parser = new Ast();
            parser.ParseLexems(list);
            // parser.Run();

            // PrintLexemList(list);

            //w celu testowania
            // Syntax syn = new Syntax();
            // syn.SyntaxCheck(list);
            /*
            var list2 = new List<Lexem>();
            ParseLine("\"hello \'a\' world!\"", list2, 0);
            PrintLexemList(list2);*/
        }

        ///<summary>
        /// Metoda przetwarza listę linii kodu na lexemy
        /// </summary>
        ///<param name="code">linie kodu jako tablica</param>
        ///<returns>lista lexemów</returns>
        public static List<Lexem> ParseCode(string[] code)
        {
            var list = new List<Lexem>();
            for (var i = 0; i < code.Length; i++)
            {
                ParseLine(code[i], list, i);
            }

            return list;
        }

        /// <summary>
        /// Metoda przetwarza jedną linię na lexemy.
        /// </summary>
        /// <param name="line">linia kodu</param>
        /// <param name="list">lista, do której mają zostać dodane lexemy</param>
        /// <param name="lineNumber">numer linii</param>
        private static void ParseLine(string line, List<Lexem> list, int lineNumber)
        {
            var s = "";
            var arr = line.Trim().ToCharArray();
            var type = CharType.Undefined;

            for (var i = 0; i < arr.Length; i++)
            {
                var c = arr[i];
                var currentType = CheckType(c);

                if (c == '!') currentType = CharType.Bracket;

                // string
                if (type == CharType.Quote || type == CharType.DoubleQuote)
                {
                    s += c;
                    if (currentType == type)
                    {
                        list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                        s = "";
                        type = CharType.Undefined;
                    }

                    continue;
                }

                // float
                if (c == '.' && type == CharType.Digit)
                {
                    s += c;
                    continue;
                }

                if (char.IsWhiteSpace(c))
                {
                    if (s.Length > 0) 
                        list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                    s = "";
                    type = CharType.Undefined;
                    continue;
                }

                if (c == '/' && i + 1 < arr.Length && arr[i + 1] == '/')
                {
                    if (s.Length > 0) 
                        list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                    return;
                }

                switch (currentType)
                {
                    case CharType.Bracket:
                        if (s.Length > 0) list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                        list.Add(new Lexem {Text = c.ToString(), LineNumber = lineNumber});
                        s = "";
                        type = CharType.Undefined;
                        break;
                    case CharType.Digit:
                        s += c;
                        if (i + 1 < arr.Length && 
                            CheckType(arr[i + 1]) != CharType.Digit && 
                            CheckType(arr[i + 1]) != CharType.Character && 
                            arr[i + 1] != '.')
                        {
                            list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                            s = "";
                            type = CharType.Undefined;
                        }
                        else
                            type = CharType.Digit;

                        break;
                    case CharType.Symbol:
                        s += c;
                        if (i + 1 < arr.Length && CheckType(arr[i + 1]) != CharType.Symbol)
                        {
                            list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                            s = "";
                            type = CharType.Undefined;
                        }
                        else
                            type = CharType.Symbol;

                        break;
                    case CharType.Quote:
                    case CharType.DoubleQuote:
                        type = currentType;
                        s += c;
                        break;
                    case CharType.Character:
                        s += c;
                        if (i + 1 < arr.Length && 
                            CheckType(arr[i + 1]) != CharType.Character && 
                            CheckType(arr[i + 1]) != CharType.Digit)
                        {
                            list.Add(new Lexem {Text = s, LineNumber = lineNumber});
                            s = "";
                            type = CharType.Undefined;
                        }
                        else
                            type = CharType.Character;

                        break;
                }
            }

            if (s.Length > 0) list.Add(new Lexem {Text = s, LineNumber = lineNumber});
        }

        ///<summary>
        /// Wypisuje listę lexemów.
        /// </summary>
        ///<param name="list">lista lexemów</param>
        public static void PrintLexemList(IEnumerable<Lexem> list)
        {
            var number = 0;
            foreach (var l in list)
            {
                if (l.LineNumber > number)
                {
                    number = l.LineNumber;
                    Console.WriteLine();
                    Console.Write(l.LineNumber);
                }

                Console.Write(@"   " + l.Text + @", ");
            }
            Console.WriteLine();
        }

        ///<summary>
        /// Sprawdza typ znaku
        /// </summary>
        ///<param name="c">znak</param>
        ///<returns>typ znaku</returns>
        private static CharType CheckType(char c)
        {
            if (IsBracket(c)) return CharType.Bracket;
            if (char.IsDigit(c)) return CharType.Digit;
            switch (c)
            {
                case '\'':
                    return CharType.Quote;
                case '\"':
                    return CharType.DoubleQuote;
                default:
                    return char.IsLetter(c) ? CharType.Character : CharType.Symbol;
            }
        }

        /// <summary>
        /// Sprawdza czy znak jest nawiasem lub średnikiem.
        /// </summary>
        /// <param name="c">znak</param>
        /// <returns>wartość logiczna</returns>
        private static bool IsBracket(char c)
        {
            return new[] {'{', '}', '[', ']', '(', ')', ';'}.Contains(c);
        }

        /// <summary>
        /// Typ wyliczeniowy używany do oznaczenia rodzaju znaku w kodzie.
        /// </summary>
        private enum CharType
        {
            Digit,
            Character,
            Symbol,
            Bracket,
            Quote,
            DoubleQuote,
            Undefined
        }
    }
}