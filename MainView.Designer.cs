﻿namespace HarryPlotterVirtualMachine
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainView));
            this.CodeArea = new System.Windows.Forms.RichTextBox();
            this.InfoTab = new System.Windows.Forms.TabControl();
            this.LogsPage = new System.Windows.Forms.TabPage();
            this.LogsInfoText = new System.Windows.Forms.RichTextBox();
            this.TerminalPage = new System.Windows.Forms.TabPage();
            this.Terminal = new System.Windows.Forms.RichTextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newProgramToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EmptyToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.UseTemplateToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.LoadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.buildToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.codeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.settingsToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AuthorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.VerionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.documentationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.StartButton = new System.Windows.Forms.Button();
            this.codeInfoGroupBox = new System.Windows.Forms.GroupBox();
            this.wordsCount = new System.Windows.Forms.TextBox();
            this.columnCount = new System.Windows.Forms.TextBox();
            this.lineCount = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.UncommentButton = new System.Windows.Forms.Button();
            this.CommentButton = new System.Windows.Forms.Button();
            this.SnippetList = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.InfoTab.SuspendLayout();
            this.LogsPage.SuspendLayout();
            this.TerminalPage.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.codeInfoGroupBox.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // CodeArea
            // 
            this.CodeArea.AcceptsTab = true;
            this.CodeArea.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.CodeArea.Location = new System.Drawing.Point(327, 136);
            this.CodeArea.Name = "CodeArea";
            this.CodeArea.Size = new System.Drawing.Size(835, 329);
            this.CodeArea.TabIndex = 0;
            this.CodeArea.Text = "";
            this.CodeArea.Click += new System.EventHandler(this.CodeArea_Click);
            this.CodeArea.TextChanged += new System.EventHandler(this.CodeArea_TextChanged);
            this.CodeArea.KeyUp += new System.Windows.Forms.KeyEventHandler(this.CodeArea_KeyUp);
            // 
            // InfoTab
            // 
            this.InfoTab.Controls.Add(this.LogsPage);
            this.InfoTab.Controls.Add(this.TerminalPage);
            this.InfoTab.Location = new System.Drawing.Point(8, 479);
            this.InfoTab.Name = "InfoTab";
            this.InfoTab.SelectedIndex = 0;
            this.InfoTab.Size = new System.Drawing.Size(1154, 300);
            this.InfoTab.TabIndex = 1;
            // 
            // LogsPage
            // 
            this.LogsPage.Controls.Add(this.LogsInfoText);
            this.LogsPage.Location = new System.Drawing.Point(4, 22);
            this.LogsPage.Name = "LogsPage";
            this.LogsPage.Padding = new System.Windows.Forms.Padding(3);
            this.LogsPage.Size = new System.Drawing.Size(1146, 274);
            this.LogsPage.TabIndex = 0;
            this.LogsPage.Text = "Logs";
            this.LogsPage.UseVisualStyleBackColor = true;
            // 
            // LogsInfoText
            // 
            this.LogsInfoText.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.LogsInfoText.Location = new System.Drawing.Point(0, 0);
            this.LogsInfoText.Name = "LogsInfoText";
            this.LogsInfoText.ReadOnly = true;
            this.LogsInfoText.Size = new System.Drawing.Size(1146, 274);
            this.LogsInfoText.TabIndex = 0;
            this.LogsInfoText.Text = "";
            this.LogsInfoText.TextChanged += new System.EventHandler(this.LogsInfoText_TextChanged);
            // 
            // TerminalPage
            // 
            this.TerminalPage.Controls.Add(this.Terminal);
            this.TerminalPage.Location = new System.Drawing.Point(4, 22);
            this.TerminalPage.Name = "TerminalPage";
            this.TerminalPage.Padding = new System.Windows.Forms.Padding(3);
            this.TerminalPage.Size = new System.Drawing.Size(1146, 274);
            this.TerminalPage.TabIndex = 1;
            this.TerminalPage.Text = "Terminal";
            this.TerminalPage.UseVisualStyleBackColor = true;
            // 
            // Terminal
            // 
            this.Terminal.Enabled = false;
            this.Terminal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Terminal.Location = new System.Drawing.Point(0, 0);
            this.Terminal.Name = "Terminal";
            this.Terminal.Size = new System.Drawing.Size(1146, 274);
            this.Terminal.TabIndex = 0;
            this.Terminal.Text = "";
            this.Terminal.TextChanged += new System.EventHandler(this.Terminal_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.buildToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.settingsToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1178, 24);
            this.menuStrip1.TabIndex = 3;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newProgramToolStripMenuItem,
            this.SaveToToolStripMenuItem,
            this.LoadToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // newProgramToolStripMenuItem
            // 
            this.newProgramToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EmptyToolStripMenuItem,
            this.UseTemplateToolStripMenuItem});
            this.newProgramToolStripMenuItem.Name = "newProgramToolStripMenuItem";
            this.newProgramToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.newProgramToolStripMenuItem.Text = "New program";
            // 
            // EmptyToolStripMenuItem
            // 
            this.EmptyToolStripMenuItem.Name = "EmptyToolStripMenuItem";
            this.EmptyToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.EmptyToolStripMenuItem.Text = "Empty";
            this.EmptyToolStripMenuItem.Click += new System.EventHandler(this.EmptyToolStripMenuItem_Click);
            // 
            // UseTemplateToolStripMenuItem
            // 
            this.UseTemplateToolStripMenuItem.Name = "UseTemplateToolStripMenuItem";
            this.UseTemplateToolStripMenuItem.Size = new System.Drawing.Size(143, 22);
            this.UseTemplateToolStripMenuItem.Text = "Use template";
            this.UseTemplateToolStripMenuItem.Click += new System.EventHandler(this.UseTemplateToolStripMenuItem_Click);
            // 
            // SaveToToolStripMenuItem
            // 
            this.SaveToToolStripMenuItem.Name = "SaveToToolStripMenuItem";
            this.SaveToToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.SaveToToolStripMenuItem.Text = "Save to";
            this.SaveToToolStripMenuItem.Click += new System.EventHandler(this.SaveToToolStripMenuItem_Click);
            // 
            // LoadToolStripMenuItem
            // 
            this.LoadToolStripMenuItem.Name = "LoadToolStripMenuItem";
            this.LoadToolStripMenuItem.Size = new System.Drawing.Size(147, 22);
            this.LoadToolStripMenuItem.Text = "Load";
            this.LoadToolStripMenuItem.Click += new System.EventHandler(this.LoadToolStripMenuItem_Click);
            // 
            // buildToolStripMenuItem
            // 
            this.buildToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.runToolStripMenuItem,
            this.buildToolStripMenuItem1});
            this.buildToolStripMenuItem.Name = "buildToolStripMenuItem";
            this.buildToolStripMenuItem.Size = new System.Drawing.Size(46, 20);
            this.buildToolStripMenuItem.Text = "Build";
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(101, 22);
            this.runToolStripMenuItem.Text = "Run";
            // 
            // buildToolStripMenuItem1
            // 
            this.buildToolStripMenuItem1.Name = "buildToolStripMenuItem1";
            this.buildToolStripMenuItem1.Size = new System.Drawing.Size(101, 22);
            this.buildToolStripMenuItem1.Text = "Build";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.codeToolStripMenuItem});
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.searchToolStripMenuItem.Text = "Search";
            // 
            // codeToolStripMenuItem
            // 
            this.codeToolStripMenuItem.Name = "codeToolStripMenuItem";
            this.codeToolStripMenuItem.Size = new System.Drawing.Size(102, 22);
            this.codeToolStripMenuItem.Text = "Code";
            // 
            // settingsToolStripMenuItem
            // 
            this.settingsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.settingsToolStripMenuItem1});
            this.settingsToolStripMenuItem.Name = "settingsToolStripMenuItem";
            this.settingsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.settingsToolStripMenuItem.Text = "Settings";
            // 
            // settingsToolStripMenuItem1
            // 
            this.settingsToolStripMenuItem1.Name = "settingsToolStripMenuItem1";
            this.settingsToolStripMenuItem1.Size = new System.Drawing.Size(116, 22);
            this.settingsToolStripMenuItem1.Text = "Settings";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AuthorsToolStripMenuItem,
            this.VerionToolStripMenuItem,
            this.documentationToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // AuthorsToolStripMenuItem
            // 
            this.AuthorsToolStripMenuItem.Name = "AuthorsToolStripMenuItem";
            this.AuthorsToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.AuthorsToolStripMenuItem.Text = "Authors";
            this.AuthorsToolStripMenuItem.Click += new System.EventHandler(this.AuthorsToolStripMenuItem_Click);
            // 
            // VerionToolStripMenuItem
            // 
            this.VerionToolStripMenuItem.Name = "VerionToolStripMenuItem";
            this.VerionToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.VerionToolStripMenuItem.Text = "Version";
            this.VerionToolStripMenuItem.Click += new System.EventHandler(this.VerionToolStripMenuItem_Click);
            // 
            // documentationToolStripMenuItem
            // 
            this.documentationToolStripMenuItem.Name = "documentationToolStripMenuItem";
            this.documentationToolStripMenuItem.Size = new System.Drawing.Size(157, 22);
            this.documentationToolStripMenuItem.Text = "Documentation";
            // 
            // StartButton
            // 
            this.StartButton.Image = ((System.Drawing.Image)(resources.GetObject("StartButton.Image")));
            this.StartButton.Location = new System.Drawing.Point(327, 43);
            this.StartButton.Name = "StartButton";
            this.StartButton.Size = new System.Drawing.Size(75, 50);
            this.StartButton.TabIndex = 4;
            this.StartButton.UseVisualStyleBackColor = true;
            this.StartButton.Click += new System.EventHandler(this.StartButton_Click);
            // 
            // codeInfoGroupBox
            // 
            this.codeInfoGroupBox.Controls.Add(this.wordsCount);
            this.codeInfoGroupBox.Controls.Add(this.columnCount);
            this.codeInfoGroupBox.Controls.Add(this.lineCount);
            this.codeInfoGroupBox.Controls.Add(this.label3);
            this.codeInfoGroupBox.Controls.Add(this.label2);
            this.codeInfoGroupBox.Controls.Add(this.label1);
            this.codeInfoGroupBox.Location = new System.Drawing.Point(520, 27);
            this.codeInfoGroupBox.Name = "codeInfoGroupBox";
            this.codeInfoGroupBox.Size = new System.Drawing.Size(299, 103);
            this.codeInfoGroupBox.TabIndex = 5;
            this.codeInfoGroupBox.TabStop = false;
            this.codeInfoGroupBox.Text = "Code info";
            // 
            // wordsCount
            // 
            this.wordsCount.Enabled = false;
            this.wordsCount.Location = new System.Drawing.Point(85, 77);
            this.wordsCount.Name = "wordsCount";
            this.wordsCount.Size = new System.Drawing.Size(197, 20);
            this.wordsCount.TabIndex = 5;
            this.wordsCount.Text = "0";
            // 
            // columnCount
            // 
            this.columnCount.Enabled = false;
            this.columnCount.Location = new System.Drawing.Point(85, 46);
            this.columnCount.Name = "columnCount";
            this.columnCount.Size = new System.Drawing.Size(197, 20);
            this.columnCount.TabIndex = 4;
            this.columnCount.Text = "0";
            // 
            // lineCount
            // 
            this.lineCount.Enabled = false;
            this.lineCount.Location = new System.Drawing.Point(85, 16);
            this.lineCount.Name = "lineCount";
            this.lineCount.Size = new System.Drawing.Size(197, 20);
            this.lineCount.TabIndex = 3;
            this.lineCount.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(22, 84);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(41, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Words:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(22, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Column:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Line:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.UncommentButton);
            this.groupBox1.Controls.Add(this.CommentButton);
            this.groupBox1.Location = new System.Drawing.Point(846, 27);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(316, 103);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Code options";
            // 
            // UncommentButton
            // 
            this.UncommentButton.Location = new System.Drawing.Point(175, 31);
            this.UncommentButton.Name = "UncommentButton";
            this.UncommentButton.Size = new System.Drawing.Size(120, 50);
            this.UncommentButton.TabIndex = 1;
            this.UncommentButton.Text = "Uncomment";
            this.UncommentButton.UseVisualStyleBackColor = true;
            this.UncommentButton.Click += new System.EventHandler(this.UncommentButton_Click);
            // 
            // CommentButton
            // 
            this.CommentButton.Location = new System.Drawing.Point(26, 31);
            this.CommentButton.Name = "CommentButton";
            this.CommentButton.Size = new System.Drawing.Size(120, 50);
            this.CommentButton.TabIndex = 0;
            this.CommentButton.Text = "Comment";
            this.CommentButton.UseVisualStyleBackColor = true;
            this.CommentButton.Click += new System.EventHandler(this.CommentButton_Click);
            // 
            // SnippetList
            // 
            this.SnippetList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.SnippetList.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.SnippetList.FormattingEnabled = true;
            this.SnippetList.ItemHeight = 25;
            this.SnippetList.Items.AddRange(new object[] {
            "start-end -> [please...thank you]",
            "string -> [czar]",
            "int -> [harry]",
            "float -> [ron]",
            "boolean -> [hermiona]",
            "true -> [fred]",
            "false - [george]",
            "if() -> [elixir(){...}]",
            "for() -> [parszywek(){...}]",
            "while() -> [hedwiga(){...}]",
            "readLine() -> [alohomora()]",
            "readIneeteger() -> [reducio()]",
            "readFloat() -> [reducto()]",
            "print(string) -> [lumos()]",
            "printLine(string) -> [accio()]",
            "toString(float/int) -> [reparo()]",
            "toInt(string) -> [veraVerto()]",
            "toFloat(string) -> [vertoVera()]",
            "exit() -> [avadaKedavra()]"});
            this.SnippetList.Location = new System.Drawing.Point(12, 136);
            this.SnippetList.Name = "SnippetList";
            this.SnippetList.Size = new System.Drawing.Size(309, 329);
            this.SnippetList.TabIndex = 7;
            this.SnippetList.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.SnippetList_MouseDoubleClick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(12, 113);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 20);
            this.label4.TabIndex = 9;
            this.label4.Text = "Snippets";
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1178, 795);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SnippetList);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.StartButton);
            this.Controls.Add(this.codeInfoGroupBox);
            this.Controls.Add(this.InfoTab);
            this.Controls.Add(this.CodeArea);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainView";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Harry Plotter";
            this.InfoTab.ResumeLayout(false);
            this.LogsPage.ResumeLayout(false);
            this.TerminalPage.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.codeInfoGroupBox.ResumeLayout(false);
            this.codeInfoGroupBox.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RichTextBox CodeArea;
        private System.Windows.Forms.TabControl InfoTab;
        private System.Windows.Forms.TabPage LogsPage;
        private System.Windows.Forms.TabPage TerminalPage;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem newProgramToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EmptyToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem UseTemplateToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem SaveToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem LoadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem buildToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem codeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem settingsToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AuthorsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem VerionToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem documentationToolStripMenuItem;
        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.GroupBox codeInfoGroupBox;
        private System.Windows.Forms.TextBox wordsCount;
        private System.Windows.Forms.TextBox columnCount;
        private System.Windows.Forms.TextBox lineCount;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button CommentButton;
        private System.Windows.Forms.Button UncommentButton;
        private System.Windows.Forms.ListBox SnippetList;
        private System.Windows.Forms.RichTextBox LogsInfoText;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.RichTextBox Terminal;
    }
}

