namespace HarryPlotterVirtualMachine.Tree
{
    /// <summary>
    /// Interfejs używany przy budowaniu operatorów infiksowych.
    /// </summary>
    /// <typeparam name="T">typ zwracanych danych</typeparam>
    public interface IBinaryOperator<T>
    {
        /// <summary>
        /// Ustawienie lewego parametru.
        /// </summary>
        /// <param name="a">węzeł wartości</param>
        void SetA(FunctionNode<T> a);

        /// <summary>
        /// Ustawienie prawego parametru.
        /// </summary>
        /// <param name="a">węzeł wartości</param>
        void SetB(FunctionNode<T> b);
    }
}