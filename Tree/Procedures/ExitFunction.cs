﻿using System.Threading;

namespace HarryPlotterVirtualMachine.Tree.Procedures
{
    public class ExitFunction : Node
    {
        private AddTextDelegete _addTextToLogs;

        public ExitFunction()
        {
        }

        public ExitFunction(AddTextDelegete addTextToLogs)
        {
            _addTextToLogs = addTextToLogs;
        }

        public override void Execute()
        {
            _addTextToLogs("Zakończono działanie");
            Thread.CurrentThread.Abort();
        }
    }
}
