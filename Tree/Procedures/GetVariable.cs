namespace HarryPlotterVirtualMachine.Tree.Procedures
{
    /// <summary>
    /// Węzeł pobierający wartość zmiennej.
    /// </summary>
    /// <typeparam name="T">typ zmiennej</typeparam>
    public class GetVariable<T> : FunctionNode<T>
    {
        /// <summary>
        /// Baza zmiennych.
        /// </summary>
        private VariableBase<T> _vb;
        /// <summary>
        /// Nazwa zmiennej.
        /// </summary>
        private string _name;
        
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="vb">baza zmiennych</param>
        /// <param name="name">nazwa zmiennej</param>
        public GetVariable(VariableBase<T> vb, string name)
        {
            _vb = vb;
            _name = name;
        }

        /// <summary>
        /// Pobranie wartości.
        /// </summary>
        public override void Execute()
        {
            Value = _vb.GetValue(_name);
        }
    }
}