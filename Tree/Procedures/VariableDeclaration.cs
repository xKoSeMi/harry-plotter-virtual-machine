namespace HarryPlotterVirtualMachine.Tree.Procedures
{
    /// <summary>
    /// Węzeł deklarujący zmienną.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class VariableDeclaration<T> : Node
    {
        /// <summary>
        /// Baza zmiennych.
        /// </summary>
        private VariableBase<T> _vb;
        /// <summary>
        /// Nazwa zmiennej.
        /// </summary>
        private string _name;
        /// <summary>
        /// Funkcja zwracająca wartość dla zmiennej.
        /// </summary>
        private FunctionNode<T> _f;

        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="vb">baza zmiennych</param>
        /// <param name="name">nazwa zmiennej</param>
        /// <param name="f">funkcja zwracająca wartość</param>
        public VariableDeclaration(VariableBase<T> vb, string name, FunctionNode<T> f)
        {
            _vb = vb;
            _name = name;
            _f = f;
            _vb.Add(name, default);
        }

        public override void Execute()
        {
            // _vb.Add(_name, _f.Value);
            _vb.SetValue(_name, _f.Value);
        }
    }
}