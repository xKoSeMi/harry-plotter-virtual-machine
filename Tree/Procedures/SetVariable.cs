namespace HarryPlotterVirtualMachine.Tree.Procedures
{
    public class SetVariable<T> : Node
    {
        /// <summary>
        /// Baza zmiennych.
        /// </summary>
        private VariableBase<T> _vb;
        /// <summary>
        /// Nazwa zmiennej.
        /// </summary>
        private string _name;
        /// <summary>
        /// Funkcja zwracająca wartość dla zmiennej.
        /// </summary>
        private FunctionNode<T> _f;
        
        /// <summary>
        /// Konstruktor.
        /// </summary>
        /// <param name="vb">baza zmiennych</param>
        /// <param name="name">nazwa zmiennej</param>
        /// <param name="f">funkcja zwracająca wartość</param>
        public SetVariable(VariableBase<T> vb, string name, FunctionNode<T> f)
        {
            _vb = vb;
            _name = name;
            _f = f;
        }

        public override void Execute()
        {
            _vb.SetValue(_name, _f.Value);
        }
    }
}