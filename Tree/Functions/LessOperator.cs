namespace HarryPlotterVirtualMachine.Tree.Functions
{
    /// <summary>
    /// Operator mniejszości int.
    /// </summary>
    public class LessInt : FunctionNode<bool>, IBinaryOperator<int>
    {
        private FunctionNode<int> _a;
        private FunctionNode<int> _b;

        public LessInt()
        {
        }

        public LessInt(FunctionNode<int> a, FunctionNode<int> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<int> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<int> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value < _b.Value;
        }
    }
    
    /// <summary>
    /// Operator mniejszości float.
    /// </summary>
    public class LessFloat : FunctionNode<bool>, IBinaryOperator<double>
    {
        private FunctionNode<double> _a;
        private FunctionNode<double> _b;

        public LessFloat()
        {
        }

        public LessFloat(FunctionNode<double> a, FunctionNode<double> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<double> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<double> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value < _b.Value;
        }
    }
}