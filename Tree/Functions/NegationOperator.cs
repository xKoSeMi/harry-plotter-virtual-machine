﻿using HarryPlotterVirtualMachine.Tree.Literals;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    public class NegationBoolOperator : FunctionNode<bool>
    {
        private readonly FunctionNode<bool> _f;

        public NegationBoolOperator(FunctionNode<bool> f)
        {
            _f = f;
        }
        
        public override void Execute()
        {
            Value = !_f.Value;
        }
    }

     public class NegationIntOperator : FunctionNode<int>
     {
         private readonly FunctionNode<int> _f;

         public NegationIntOperator(FunctionNode<int> f)
         {
             _f = f;
         }
         
         public override void Execute()
         {
             Value = -_f.Value;
         }
     }

    public class NegationFloatOperator : FunctionNode<double>
    {
        private readonly FunctionNode<double> _f;

        public NegationFloatOperator(FunctionNode<double> f)
        {
            _f = f;
        }
        
        public override void Execute()
        {
            Value = -_f.Value;
        }
    }
    
    public class NegationStringOperator : FunctionNode<string>
    {
        private readonly FunctionNode<string> _f;

        public NegationStringOperator(FunctionNode<string> f)
        {
            _f = f;
        }
        
        public override void Execute()
        {
            Value = "nie "+_f.Value;
        }
    }
}
