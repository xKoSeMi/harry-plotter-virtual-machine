﻿namespace HarryPlotterVirtualMachine.Tree.Functions
{
    /// <summary>
    /// Klasy węzłów sumujących dla różnych typów danych.
    /// </summary>
    public class SumFloat : FunctionNode<double>, IBinaryOperator<double>
    {
        private FunctionNode<double> _a;
        private FunctionNode<double> _b;

        public SumFloat()
        {
        }

        public SumFloat(FunctionNode<double> a, FunctionNode<double> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<double> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<double> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value + _b.Value;
        }
    }

    public class SumInt : FunctionNode<int>, IBinaryOperator<int>
    {
        private FunctionNode<int> _a;
        private FunctionNode<int> _b;

        public SumInt()
        {
        }

        public SumInt(FunctionNode<int> a, FunctionNode<int> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<int> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<int> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            
            Value = _a.Value + _b.Value;
        }
    }

    public class Concat : FunctionNode<string>, IBinaryOperator<string>
    {
        private FunctionNode<string> _a;
        private FunctionNode<string> _b;

        public Concat()
        {
        }

        public Concat(FunctionNode<string> a, FunctionNode<string> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<string> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<string> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value + _b.Value;
        }
    }
    
    public class Or : FunctionNode<bool>, IBinaryOperator<bool>
    {
        private FunctionNode<bool> _a;
        private FunctionNode<bool> _b;

        public Or()
        {
        }

        public Or(FunctionNode<bool> a, FunctionNode<bool> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<bool> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<bool> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value || _b.Value;
        }
    }
}