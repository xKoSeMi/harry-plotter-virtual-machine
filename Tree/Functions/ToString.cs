﻿using System.Globalization;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    /// <summary>
    /// Klasa implementująca funkcję ToString.
    /// </summary>
    /// <typeparam name="T">typ parametru</typeparam>
    public class ToString<T> : FunctionNode<string>
    {
        private readonly FunctionNode<T> _f;

        public ToString(FunctionNode<T> f)
        {
            _f = f;
        }

        /// <summary>
        /// Konwersja parametru na string.
        /// </summary>
        public override void Execute()
        {
            switch (_f)
            {
                case FunctionNode<bool> b:
                    Value = b.Value ? KeyWords.TRUE : KeyWords.FALSE;
                    break;
                case FunctionNode<double> b:
                    Value = b.Value.ToString(CultureInfo.InvariantCulture);
                    break;
                default:
                    Value = _f.Value.ToString();
                    break;
            }
        }
    }
}