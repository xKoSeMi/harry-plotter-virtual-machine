﻿using System.Globalization;
using System.Threading.Tasks;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    public class ReadFloat : FunctionNode<double>
    {
        /// <summary>
        /// Delegat zmieniający status terminala
        /// </summary>
        ChangeEnabledTerminalStatusDelegate _changeTerminalStatus;

        /// <summary>
        /// Delegat zwracający wpisany tekst przez użytkownika, w momencie naciśnięcia przez niego enter
        /// </summary>
        GetTypedTextDelegate _getTypedText;

        /// <summary>
        /// Konstruktor przekazuący metody do modyfikacji i pobierania tekstu z terminala.
        /// </summary>
        /// <param name="changeTerminalStatus">Delegat do zmiany statusu terminala</param>
        /// <param name="getTypedText">Delegat zwracający wpisany tekst przez użytkownika, w momencie naciśnięcia przez niego enter</param>
        public ReadFloat(ChangeEnabledTerminalStatusDelegate changeTerminalStatus, GetTypedTextDelegate getTypedText)
        {
            _changeTerminalStatus = changeTerminalStatus;
            _getTypedText = getTypedText;
        }
        public override void Execute()
        {
            _changeTerminalStatus();
            var getTextFromTerminal = Task.Run(() =>
            {
                Value = double.Parse(_getTypedText(), CultureInfo.InvariantCulture);
            });
            getTextFromTerminal.Wait();
            _changeTerminalStatus();
        }
    }
}
