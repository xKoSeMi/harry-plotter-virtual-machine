﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    public class RestOfDivision : FunctionNode<int>, IBinaryOperator<int>
    {
        private FunctionNode<int> _a;
        private FunctionNode<int> _b;

        public void SetA(FunctionNode<int> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<int> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value % _b.Value;
        }
    }
}
