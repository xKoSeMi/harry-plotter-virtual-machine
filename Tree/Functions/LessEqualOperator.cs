﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    public class LessEqualInt : FunctionNode<bool>, IBinaryOperator<int>
    {
        private FunctionNode<int> _a;
        private FunctionNode<int> _b;

        public void SetA(FunctionNode<int> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<int> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value <= _b.Value;
        }
    }

    public class LessEqualFloat : FunctionNode<bool>, IBinaryOperator<double>
    {
        private FunctionNode<double> _a;
        private FunctionNode<double> _b;

        public LessEqualFloat()
        {
        }

        public LessEqualFloat(FunctionNode<double> a, FunctionNode<double> b)
        {
            _a = a;
            _b = b;
        }

        public void SetA(FunctionNode<double> a)
        {
            _a = a;
        }

        public void SetB(FunctionNode<double> b)
        {
            _b = b;
        }

        public override void Execute()
        {
            Value = _a.Value <= _b.Value;
        }
    }
}
