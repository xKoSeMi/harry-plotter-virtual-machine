﻿using System;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    class ToFloat<T> : FunctionNode<double>
    {
        private readonly FunctionNode<T> _f;

        public ToFloat(FunctionNode<T> f)
        {
            _f = f;
        }

        public override void Execute()
        {
            switch (_f)
            {
                case FunctionNode<string> _:
                case FunctionNode<int> _:
                    Value = Convert.ToDouble(_f.Value);
                    break;
            }
        }
    }
}
