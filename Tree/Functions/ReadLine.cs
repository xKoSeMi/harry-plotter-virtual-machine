﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HarryPlotterVirtualMachine.Tree.Functions
{
    class ReadLine : FunctionNode<string>
    {
        /// <summary>
        /// Delegat zmieniający status terminala
        /// </summary>
        ChangeEnabledTerminalStatusDelegate _changeTerminalStatus;

        /// <summary>
        /// Delegat zwracający wpisany tekst przez użytkownika, w momencie naciśnięcia przez niego enter
        /// </summary>
        GetTypedTextDelegate _getTypedText;

        /// <summary>
        /// Konstruktor przekazuący metody do modyfikacji i pobierania tekstu z terminala.
        /// </summary>
        /// <param name="changeTerminalStatus">Delegat do zmiany statusu terminala</param>
        /// <param name="getTypedText">Delegat zwracający wpisany tekst przez użytkownika, w momencie naciśnięcia przez niego enter</param>
        public ReadLine(ChangeEnabledTerminalStatusDelegate changeTerminalStatus, GetTypedTextDelegate getTypedText)
        {
            _changeTerminalStatus = changeTerminalStatus;
            _getTypedText = getTypedText;
        }
        public override void Execute()
        {
            _changeTerminalStatus();
            Task getTextFromTerminal = Task.Run(() =>
            {
                Value = _getTypedText();
            });
            getTextFromTerminal.Wait();
            _changeTerminalStatus();
        }
    }
}
