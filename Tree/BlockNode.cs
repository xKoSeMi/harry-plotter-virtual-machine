namespace HarryPlotterVirtualMachine.Tree
{
    /// <summary>
    /// Węzeł podstawowy dla funkcji z blokiem kodu (for, while, if).
    /// </summary>
    public abstract class BlockNode:Node
    {
        /// <summary>
        /// Blok kodu do wykonania.
        /// </summary>
        protected Node Block;

        /// <summary>
        /// Pętla wykonująca blok.
        /// </summary>
        /// <param name="start">początkowy węzeł</param>
        protected static void _executeBlock(Node start)
        {
            var n = start;
            while (n != null)
            {
                n.Execute();
                n = n.Next;
            }
        }
    }
}