﻿using System;

namespace HarryPlotterVirtualMachine.Tree
{
    /// <summary>
    /// Klasa podstawowa dla węzłów w drzewie.
    /// </summary>
    public abstract class Node
    {
        /// <summary>
        /// Właściwość przechowująca następne polecenie równoległe
        /// </summary>
        public Node Next { get; set; }

        /// <summary>
        /// Wykonanie operacji aktualnego węzła.
        /// </summary>
        public abstract void Execute();
    }
}