﻿namespace HarryPlotterVirtualMachine.Tree.Literals
{
    /// <summary>
    /// Funkcja pełniąca rolę wartości dosłownej int.
    /// </summary>
    public class IntLiteral :FunctionNode<int>
    {
        /// <summary>
        /// Jedyny konstruktor
        /// </summary>
        /// <param name="value">napis do przechowania</param>
        public IntLiteral(int value)
        {
            Value = value;
        }

        /// <summary>
        /// <inheritdoc cref="Node"/>
        /// </summary>
        public override void Execute()
        {
        }
    }
}