namespace HarryPlotterVirtualMachine.Tree.Literals
{
    /// <summary>
    /// Funkcja pełniąca rolę wartości dosłownej boolean.
    /// </summary>
    public class BooleanLiteral : FunctionNode<bool>
    {
        /// <summary>
        /// Jedyny konstruktor
        /// </summary>
        /// <param name="value">napis do przechowania</param>
        public BooleanLiteral(bool value)
        {
            Value = value;
        }

        /// <summary>
        /// <inheritdoc cref="Node"/>
        /// </summary>
        public override void Execute()
        {
        }
    }
}