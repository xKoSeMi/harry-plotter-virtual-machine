﻿namespace HarryPlotterVirtualMachine.Tree.Blocks
{
    /// <summary>
    /// Węzeł reprezentujący pętlę while.
    /// </summary>
    public class While : BlockNode
    {
        /// <summary>
        /// Funkcja sprawdzająca wartość warunku.
        /// </summary>
        private readonly FunctionNode<bool> _condition;

        public While(FunctionNode<bool> condition, Node block)
        {
            _condition = condition;
            Block = block;
        }

        public override void Execute()
        {
            while (_condition.Value)
            {
                _executeBlock(Block);
            }
        }
    }
}