﻿namespace HarryPlotterVirtualMachine.Tree.Blocks
{
    /// <summary>
    /// Węzeł reprezentujący pętlę for.
    /// </summary>
    public class For : BlockNode
    {
        /// <summary>
        /// Funkcja sprawdzająca wartość warunku.
        /// </summary>
        private readonly FunctionNode<bool> _condition;
        /// <summary>
        /// Funkcja deklarująca zmienną iteracyjną.
        /// </summary>
        private readonly Node _declaration;
        /// <summary>
        /// Funkcja zmieniająca wartość zmiennej iteracyjnej.
        /// </summary>
        private readonly Node _change;

        public For(Node declaration, FunctionNode<bool> condition, Node change, Node block)
        {
            _condition = condition;
            _declaration = declaration;
            _change = change;
            Block = block;
        }

        public override void Execute()
        {
            _declaration?.Execute();
            while (_condition == null || _condition.Value)
            {
                _executeBlock(Block);
                _change?.Execute();
            }
        }
    }
}