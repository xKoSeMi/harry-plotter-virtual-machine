namespace HarryPlotterVirtualMachine.Tree.Blocks
{
    /// <summary>
    /// Węzeł reprezentujący instruckję warunkową if...else.
    /// </summary>
    public class If : BlockNode
    {
        /// <summary>
        /// Funkcja sprawdzająca wartość warunku.
        /// </summary>
        private readonly FunctionNode<bool> _condition;
        /// <summary>
        /// Blok wykonywany przy fałszywym warunku.
        /// </summary>
        private readonly Node _elseBlock;

        public If(FunctionNode<bool> condition, Node block)
        {
            _condition = condition;
            Block = block;
        }

        public If(FunctionNode<bool> condition, Node block, Node elseBlock)
        {
            _condition = condition;
            Block = block;
            _elseBlock = elseBlock;
        }

        public override void Execute()
        {
            if (_condition.Value)
                _executeBlock(Block);
            else if(_elseBlock != null)
                _executeBlock(_elseBlock);
        }
    }
}