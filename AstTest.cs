﻿using System;
using System.Collections.Generic;
using HarryPlotterVirtualMachine.Tree;
using HarryPlotterVirtualMachine.Tree.Blocks;
using HarryPlotterVirtualMachine.Tree.Functions;
using HarryPlotterVirtualMachine.Tree.Literals;
using HarryPlotterVirtualMachine.Tree.Procedures;

namespace HarryPlotterVirtualMachine
{
    /// <summary>
    /// Klasa do testowania struktury drzewa Abstract Syntax Tree.
    /// </summary>
    public static class AstTest
    {
        /// <summary>
        /// Metoda do testów parsowania.
        /// </summary>
        public static void Test()
        {
            var lines = System.IO.File.ReadAllLines("../../Code.hpvm");
            var lexems = Lexem.ParseCode(lines);
            Lexem.PrintLexemList(lexems);
            var syntax = new Syntax();
            syntax.SyntaxCheck(lexems, Console.WriteLine);

            var parser = new Ast(Lexem.ParseCode(lines), syntax.VariableList, Console.WriteLine, null,null);
            parser.Run();
        }

        /// <summary>
        /// Wywołanie z formularza
        /// </summary>
        /// <param name="codeLines">przesłane linie kodu z pola</param>
        /// <param name="addTextToLogs">delegat do wypisywania wyniku metody PrintLine</param>
        /// <param name="changeTerminalStatus">delegat do zmiany statusu terminala</param>
        public static void Start(string[] codeLines, AddTextDelegete addTextToLogs, GetTypedTextDelegate getTypedText,
            ChangeEnabledTerminalStatusDelegate changeTerminalStatus)
        {
            var lexemList = Lexem.ParseCode(codeLines);

            var syn = new Syntax();

            if (!syn.SyntaxCheck(lexemList, addTextToLogs)) return;
            var parser = new Ast(lexemList, syn.VariableList, addTextToLogs, getTypedText, changeTerminalStatus);
            parser.Run();
        }

        private static void ForLoopTest()
        {
            var intBase = new VariableBase<int>();
            var list = new List<Node>();

            var declare = intBase.Declare("i", new IntLiteral(0));
            var condition = new LessInt(intBase.Get("i"), new IntLiteral(5));
            var change = intBase.Set("i", new SumInt(intBase.Get("i"), new IntLiteral(1)));

            var block = new PrintLine(new ToString<int>(intBase.Get("i")));
            list.Add(new For(declare, condition, change, block));

            var start = Build(list);
            Run(start);
        }

        private static void WhileLoopTest()
        {
            var intBase = new VariableBase<int>();

            var list = new List<Node>();
            var declare = intBase.Declare("i", new IntLiteral(0));
            var condition = new LessInt(intBase.Get("i"), new IntLiteral(5));

            var block = new PrintLine(new ToString<int>(intBase.Get("i")))
            {
                Next = intBase.Set("i", new SumInt(intBase.Get("i"), new IntLiteral(1)))
            };

            list.Add(declare);
            list.Add(new While(condition, block));

            var start = Build(list);
            Run(start);
        }

        private static void VariableTest()
        {
            var intBase = new VariableBase<int>();

            var list = new List<Node>
            {
                intBase.Declare("a", new IntLiteral(1)), intBase.Declare("b", new IntLiteral(2))
            };

            var concat = new Concat(new StringLiteral("\"a = \""), new ToString<int>(intBase.Get("a")));
            var concat2 = new Concat(new StringLiteral("\"b = \""), new ToString<int>(intBase.Get("b")));
            list.Add(new PrintLine(concat));
            list.Add(new PrintLine(concat2));

            var sum = new SumInt(intBase.Get("a"), intBase.Get("b"));
            list.Add(intBase.Set("a", sum));

            var concat3 = new Concat(new StringLiteral("\"a = \""), new ToString<int>(intBase.Get("a")));
            list.Add(new PrintLine(concat3));

            var start = Build(list);
            Run(start);
        }

        private static void Run(Node start)
        {
            var n = start;
            while (n != null)
            {
                n.Execute();
                n = n.Next;
            }
        }

        private static Node Build(IReadOnlyList<Node> list)
        {
            var start = list[0];
            var n = start;

            for (var i = 1; i < list.Count; i++)
            {
                n.Next = list[i];
                n = n.Next;
            }

            return start;
        }
    }
}
