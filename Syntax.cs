﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace HarryPlotterVirtualMachine
{
    ///<summary>
    /// Klasa odpowiedzialna za sprawdzanie składni
    /// </summary>
    public class Syntax
    {
        public readonly List<Variable> VariableList = new List<Variable>();

        //Błedy struktury
        string KeyWordsException = "Niepoprawna struktura.";
        string WrongTypeVariable = "Zły typ danych.";
        string WrongBrackets = "Niepoprawne klamry";

        //Błędy zmiennych 
        string NameNumberException = "Nazwa zmiennej nie może zaczynać się od liczby.";
        string VariableException = "Taka zmienna już istnieje.";
        string VariableMissingException = "Taka zmienna nie istnieje.";
        string WrongExpressionException = "Niepoprawne wyrażenie.";
        string WrongBlokException = "Niepoprawny blok kodu.";

        ///<summary>
        /// Sprawdza poprawnoś lexemów.
        /// </summary>
        ///<param name="list">lista lexemów</param>
        ///<param name="addTextToLogs">delegat do metody wypisywania w logach</param>
        public bool SyntaxCheck(List<Lexem> list, AddTextDelegete addTextToLogs)
        {
            //gdy uruchomimy pusty program
            if (list.Count == 0)
            {
                return false;
            }

            var wordsCountLine = _countWords(list);

            if (!list.First().Text.Equals(KeyWords.START))
            {
                addTextToLogs("Linia: " + list.First().LineNumber + ". Błąd: " + KeyWordsException);
                return false;
            }

            //sprawdzanie czy na koncu jest thank you
            if (!list.Last().Text.Equals(KeyWords.END2)
                || !list.ElementAt(list.Count - 2).Text.Equals(KeyWords.END1))
            {
                addTextToLogs("Linia: " + list.Last().LineNumber + ". Błąd: " + KeyWordsException);
                return false;
            }

            var firstIndex = list.First().LineNumber;
            var lastIndex = list.Last().LineNumber;

            var backup = 0;
            if (!_checkBracket(list, ref backup, '{', '}', false, false))
            {
                addTextToLogs("Linia: " + list[backup].LineNumber + ". Błąd: " + WrongBrackets);
                return false;
            }

            string name;
            Type type;

            for (var index = 0; index < list.Count; index++)
            {
                var l = list[index];
                switch (l.Text)
                {
                    case KeyWords.START:
                        //sprawdzamy czy please znajduje się na początu, w pierwszej linii
                        if (firstIndex != l.LineNumber || wordsCountLine[firstIndex] != 1)
                        {
                            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + KeyWordsException);
                            //przerwanie programu
                            return false;
                        }

                        break;

                    case KeyWords.END1:
                        //sprawdzeni cz jest dobre zakonczenie programu thank
                        if (lastIndex != l.LineNumber || wordsCountLine[lastIndex] != 2)
                        {
                            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + KeyWordsException);
                            //przerwanie programu
                            return false;
                        }

                        break;
                    case KeyWords.END2:
                        //sprawdzeni cz jest dobre zakonczenie programu you
                        if (lastIndex != l.LineNumber || wordsCountLine[lastIndex] != 2)
                        {
                            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + KeyWordsException);
                            //przerwanie programu
                            return false;
                        }

                        break;
                    case KeyWords.INT:
                        index++;
                        name = list[index].Text;
                        if (!_checkVariableDeclaration(wordsCountLine, list, ref index,
                            typeof(int), addTextToLogs))
                            return false;

                        VariableList.Add(new Variable(name, typeof(int)));
                        break;

                    case KeyWords.STRING:
                        index++;
                        name = list[index].Text;
                        if (!_checkVariableDeclaration(wordsCountLine, list, ref index,
                            typeof(string), addTextToLogs))
                            return false;

                        VariableList.Add(new Variable(name, typeof(string)));
                        break;

                    case KeyWords.FLOAT:
                        index++;
                        name = list[index].Text;
                        if (!_checkVariableDeclaration(wordsCountLine, list, ref index,
                            typeof(float), addTextToLogs))
                            return false;

                        VariableList.Add(new Variable(name, typeof(float)));
                        break;

                    case KeyWords.BOOLEAN:
                        index++;
                        name = list[index].Text;
                        if (!_checkVariableDeclaration(wordsCountLine, list, ref index,
                            typeof(bool), addTextToLogs))
                            return false;

                        VariableList.Add(new Variable(name, typeof(bool)));
                        break;

                    case KeyWords.WHILE:
                    case KeyWords.IF:
                        if (!_checkIf(list, ref index, addTextToLogs))
                            return false;
                        break;
                    case KeyWords.FOR:
                        if (!_checkFor(list, ref index, addTextToLogs, wordsCountLine))
                            return false;
                        break;
                    case "{":
                    case "}":
                        break;
                    case KeyWords.PRINT:
                    case KeyWords.PRINT_LINE:
                        if (!list[index + 1].Text.Equals("("))
                        {
                            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + KeyWordsException);
                            return false;
                        }
                        index+=2;
                        type = _checkExpressionType(list, ref index, true, false);
                        if (type != typeof(string))
                        {
                            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongExpressionException);
                            return false;
                        }
                        break;
                    default:
                        type = Ast.GetType(l, VariableList);
                        if (type == null)
                        {
                            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + VariableMissingException);
                            return false;
                        }

                        if (Ast.IsVariable(l, VariableList)
                            && !_checkVariableDeclaration(wordsCountLine, list,
                                ref index,
                                type, addTextToLogs, true))
                            return false;

                        break;
                }
            }

            return true;
        }

        /// <summary>
        /// Sprawdzanie poprawność deklaracji zmiennej.
        /// </summary>
        /// <param name="wordsCountLine">lista liczb słów w liniach</param>
        /// <param name="list">lista leksemów</param>
        /// <param name="index">index listy</param>
        /// <param name="t">oczekiwany typ zmiennej</param>
        /// <param name="addTextToLogs">delegat do wypisania komunikatów</param>
        /// <param name="existingVariable">czy to deklaracja nowej zmiennej
        /// czy przypisanie wartości do istniejącej</param>
        /// <returns>wartość logiczna</returns>
        private bool _checkVariableDeclaration(IReadOnlyList<int> wordsCountLine, List<Lexem> list,
            ref int index, Type t, AddTextDelegete addTextToLogs, bool existingVariable = false, bool loop = false)
        {
            var l = list[index];
            var name = l.Text;

            //sprawdzanie czy poprawny zapis zmiennej
            if (!CheckSchemaVariable(list, index, addTextToLogs))
            {
                return false;
            }

            // sprawdzamy czy wogóle jest deklaracja zmiennej
            if (wordsCountLine[l.LineNumber] < 4 && !existingVariable
                || wordsCountLine[l.LineNumber] < 3 || !list[index + 1].Text.Equals("="))
            {
                addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + KeyWordsException);
                return false;
            }

            if (VariableList.Count(v => v.Name.Equals(name)) > 0 && !existingVariable)
            {
                addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + VariableException);
                return false;
            }

            //sprawdzanie czy jest poprawny zapis danych 
            index += 2;
            var type = _checkExpressionType(list, ref index, true, false);

            if (type == null)
            {
                addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongExpressionException);
                return false;
            }

            if (type == t) return true;
            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongTypeVariable);
            return false;
        }

        /// <summary>
        /// Metoda licząca liczbę słów w każdej linii.
        /// </summary>
        /// <param name="list">lista lexemów</param>
        /// <returns>lista liczb</returns>
        private static List<int> _countWords(IEnumerable<Lexem> list)
        {
            var wordsCountLine = new List<int>();
            var line = 0;
            wordsCountLine.Add(0);

            foreach (var l in list)
            {
                if (l.LineNumber > line)
                {
                    if (l.LineNumber - line > 1)
                    {
                        for (var i = line + 1; i < l.LineNumber; i++)
                        {
                            wordsCountLine.Add(0);
                        }
                    }

                    wordsCountLine.Add(1);
                    line = l.LineNumber;
                }
                else if (l.LineNumber == line)
                {
                    wordsCountLine[line]++;
                }
            }

            return wordsCountLine;
        }

        /// <summary>
        ///  Funkcja sprawdzająca poprawny zapis zmiennych.
        ///  </summary>
        /// <param name="list">Lista lexemow</param>
        /// <param name="index">informacja o index</param>
        ///<param name="addTextToLogs">Delegat do wypisywania w okinku log</param>
        public bool CheckSchemaVariable(List<Lexem> list, int index, AddTextDelegete addTextToLogs)
        {
            //sprawdzamy czy nazwa zmiennej nie jest pusta i nie zaczyna się od liczby
            if (list[index].Text != null && char.IsNumber(list[index].Text, 0))
            {
                addTextToLogs("Linia: " + list[index].LineNumber + ". Błąd: " + NameNumberException);
                return false;
            }

            //sprawdzamy czy po nazwie zmiennej występuje znak =
            if (list[index + 1].Text.Equals("=")) return true;

            addTextToLogs("Linia: " + list[index].LineNumber + ". Błąd: " + KeyWordsException);
            return false;
        }

        /// <summary>
        /// Metoda sprawdzająca składnię instrukcji if oraz while.
        /// </summary>
        /// <param name="list">lista leksemów</param>
        /// <param name="index">index listy</param>
        /// <param name="addTextToLogs">delegat do wypisania błędów</param>
        /// <returns>wartość logiczna</returns>
        private bool _checkIf(IReadOnlyList<Lexem> list, ref int index, AddTextDelegete addTextToLogs)
        {
            // nawias otwarty,
            var l = list[index + 1];
            if (!l.Text.Equals("(")) return false;

            // wyrażenie logiczne aż do nawiasu zamkniętego,
            index += 2;
            var backup = index;
            if (!_checkBracket(list, ref backup, '(', ')', true, true))
            {
                addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongExpressionException);
                return false;
            }
            
            // index++;
            var t = _checkExpressionType(list, ref index, true, false);
            if (t != typeof(bool))
            {
                addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongExpressionException);
                return false;
            }

            index++;
            if (list[index].Text.Equals("{")) return true;

            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongBlokException);
            return false;
        }

        /// <summary>
        /// Metoda sprawdzająca składnię pętli for.
        /// </summary>
        /// <param name="list">lista leksemów</param>
        /// <param name="index">index listy</param>
        /// <param name="addTextToLogs">delegat do wypisania błędów</param>
        /// <param name="wordsCountLine">lista liczb słów w liniach</param>
        /// <returns></returns>
        private bool _checkFor(List<Lexem> list, ref int index, AddTextDelegete addTextToLogs,
            IReadOnlyList<int> wordsCountLine)
        {
            if (!list[index + 1].Text.Equals("("))
            {
                return false;
            }

            index += 2;
            var l = list[index];
            Type t;
            var existingVar = false;
            var empty = false;
            switch (l.Text)
            {
                case KeyWords.INT:
                    t = typeof(int);
                    break;
                case KeyWords.FLOAT:
                    t = typeof(float);
                    break;
                case KeyWords.STRING:
                    t = typeof(string);
                    break;
                case KeyWords.BOOLEAN:
                    t = typeof(bool);
                    break;
                case ";":
                    t = null;
                    empty = true;
                    break;
                default:
                    t = Ast.GetType(l, VariableList);
                    if (t != null)
                        existingVar = true;
                    break;
            }

            // sprawdzanie deklaracji
            if (!empty)
            {
                var nameIndex = ++index;
                if (t == null || !_checkVariableDeclaration(wordsCountLine, list,
                    ref index, t, addTextToLogs, existingVar, true))
                {
                    addTextToLogs("Linia: " + l.LineNumber + ". Błąd: Niepoprawna deklaracja zmiennej.");
                    return false;
                }

                var name = list[nameIndex].Text;
                VariableList.Add(new Variable(name, t));
                index++;
            }

            // sprawdzenie warunku
            index++;
            if (list[index].Text[0] != ';')
            {
                t = _checkExpressionType(list, ref index, false, false);

                if (t != typeof(bool))
                {
                    addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongTypeVariable);
                    return false;
                }

                index++;
            }

            // sprawdzenie modyfikacji
            index++;
            l = list[index];
            if (!l.Text.Equals(")"))
            {
                t = Ast.GetType(l, VariableList);
                var variableExist = Ast.IsVariable(l, VariableList);

                if (!variableExist || !_checkVariableDeclaration(wordsCountLine, list,
                    ref index, t, addTextToLogs, true))
                {
                    addTextToLogs("Linia: " + l.LineNumber + ". Błąd: Niepoprawne wyrażenie.");
                    return false;
                }
            }

            // klamry
            index++;
            if (list[index].Text.Equals("{")) return true;
            addTextToLogs("Linia: " + l.LineNumber + ". Błąd: " + WrongBlokException);
            return false;
        }

        /// <summary>
        /// Metoda sprawdzająca nawiasy
        /// </summary>
        /// <param name="list">lista leksemów</param>
        /// <param name="index">index listy</param>
        /// <param name="c1">nawias otwierający</param>
        /// <param name="c2">nawias zamykający</param>
        /// <param name="onlyOneLine">tylko do końca linii</param>
        /// <param name="insideBracket">wyrażenie wewnątrz nawiasu</param>
        /// <returns>wartość logiczna</returns>
        private static bool _checkBracket(IReadOnlyList<Lexem> list, ref int index,
            char c1, char c2, bool onlyOneLine, bool insideBracket)
        {
            var startLine = list[index].LineNumber;
            var stack = new Stack<bool>();
            if (insideBracket) stack.Push(true);

            while (true)
            {
                if (index >= list.Count) break;
                var l = list[index];
                if (onlyOneLine && startLine != l.LineNumber) break;

                var tmp = l.Text[0];
                if (tmp == c1)
                    stack.Push(true);
                else if (tmp == c2)
                {
                    if (stack.Count < 1) return false;
                    stack.Pop();
                }

                index++;
            }

            return stack.Count == 0;
        }

        /// <summary>
        /// Metoda sprawdzająca typ wyrażenia
        /// </summary>
        /// <param name="list">lista leksemów</param>
        /// <param name="index">index listy</param>
        /// <param name="bracket">czy wyrażenie w nawiasie</param>
        /// <param name="math">czy wyrażenie matematyczne</param>
        /// <returns>typ wyrażenia</returns>
        private Type _checkExpressionType(IReadOnlyList<Lexem> list, ref int index,
            bool bracket, bool math)
        {
            Type past = null;
            var isPastOperator = false;
            var startingLine = list[index].LineNumber;
            while (true)
            {
                var l = list[index];
                if (l.LineNumber != startingLine)
                {
                    index--;
                    return past;
                }

                Type t;
                switch (l.Text)
                {
                    case KeyWords.TRUE:
                    case KeyWords.FALSE:
                        if (past != null && past != typeof(bool)) return null;
                        past = typeof(bool);
                        isPastOperator = false;
                        break;
                    case "(":
                        index++;
                        t = _checkExpressionType(list, ref index, true, false);
                        if (past != null && past != t || t == null) return null;
                        past = t;
                        break;
                    case ")":
                        if (!bracket) index--;
                        return past;
                    case ";":
                        index--;
                        return past;
                    case "&&":
                    case "||":
                        if (math)
                        {
                            index--;
                            return past;
                        }

                        if (past != typeof(bool)) return null;
                        past = typeof(bool);
                        isPastOperator = true;
                        break;
                    case "==":
                    case "!=":
                        if (math)
                        {
                            index--;
                            return past;
                        }

                        index++;
                        t = _checkExpressionType(list, ref index, false, false);
                        if (past == null || past == typeof(bool) || past != t)
                            return null;
                        past = typeof(bool);
                        isPastOperator = true;
                        break;
                    case "+":
                    case "-":
                    case "*":
                    case "/":
                    case "%":
                        index++;
                        t = _checkExpressionType(list, ref index, false, true);

                        if (past == null && l.Text.Equals("-"))
                        {
                            if (t != typeof(int) && t != typeof(float))
                                return null;
                            past = t;
                        }
                        else
                        {
                            if (past == null
                                || past != t
                                || past == typeof(bool)
                                || (past == typeof(string) && !l.Text.Equals("+")))
                                return null;
                            past = t;
                            isPastOperator = true;
                        }

                        break;
                    case "<":
                    case "<=":
                    case ">":
                    case ">=":
                        if (math)
                        {
                            index--;
                            return past;
                        }

                        index++;
                        t = _checkExpressionType(list, ref index, false, false);
                        if (past == null || past != t || past == typeof(bool)
                            || past == typeof(string))
                            return null;
                        // index--;
                        past = typeof(bool);
                        isPastOperator = true;
                        break;
                    case "!":
                        if (math)
                        {
                            index--;
                            return past;
                        }

                        index++;
                        t = _checkExpressionType(list, ref index, false, false);
                        if (t == typeof(int) || t == typeof(float))
                            return null;
                        past = typeof(bool);
                        break;
                    default:
                        // TODO sprawdzenie funkcji
                        t = _isFunction(l) ? _checkFunction(list, ref index) : Ast.GetType(l, VariableList);

                        if (past != null
                            && t != null
                            && past != t) return null;
                        if (past != null && !isPastOperator) return null;
                        past = t;
                        isPastOperator = false;
                        break;
                }

                index++;
            }
        }

        /// <summary>
        /// Metoda do sprawdzania poprawności wywołania funkcji.
        /// </summary>
        /// <param name="list">lista leksemów</param>
        /// <param name="index">index listy</param>
        /// <returns>typ</returns>
        private Type _checkFunction(IReadOnlyList<Lexem> list, ref int index)
        {
            var l = list[index];
            var empty = false;
            Type t = null;
            if (!list[index + 1].Text.Equals("(")) return null;
            
            index += 2;
            if (list[index].Text.Equals(")")) 
                empty = true;
            else
                t = _checkExpressionType(list, ref index, true, false);

            switch (l.Text)
            {
                case KeyWords.READ_LINE:
                    return !empty ? null : typeof(string);
                case KeyWords.READ_INTEGER:
                    return !empty ? null : typeof(int);
                case KeyWords.READ_FLOAT:
                    return !empty ? null : typeof(float);
                case KeyWords.TO_STRING:
                    return t == null ? null : typeof(string);
                case KeyWords.TO_INT:
                    return t != typeof(float) && t != typeof(string) ? null : typeof(int);
                case KeyWords.TO_FLOAT:
                    return t != typeof(int) && t != typeof(string) ? null : typeof(float);
                default:
                    return null;
            }
        }

        /// <summary>
        /// Metoda do sprawdzenia czy leksem oznacza funkcję.
        /// </summary>
        /// <param name="l">leksem</param>
        /// <returns>wartość logiczna</returns>
        private static bool _isFunction(Lexem l)
        {
            return KeyWords.FUNCTIONS_ARRAY.Contains(l.Text);
        }
    }
}
