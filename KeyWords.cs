﻿namespace HarryPlotterVirtualMachine
{
    public static class KeyWords
    {
        public const string START = "please";
        public const string END1 = "thank";
        public const string END2 = "you";

        public const string TRUE = "fred";
        public const string FALSE = "george";
        
        public const string STRING = "czar";
        public const string INT = "harry";
        public const string FLOAT = "ron";
        public const string BOOLEAN = "hermiona";
        
        public const string READ_LINE = "alohomora";
        public const string READ_INTEGER = "reducio";
        public const string READ_FLOAT = "reducto";
        public const string PRINT = "lumos";
        public const string PRINT_LINE = "accio";
        public const string EXIT = "avadaKedavra";
        
        public const string TO_STRING = "reparo";
        public const string TO_INT = "veraVerto";
        public const string TO_FLOAT = "vertoVera";
        
        public const string IF = "elixir";
        public const string ELSE = "reverte";
        public const string FOR = "parszywek";
        public const string WHILE = "hedwiga";

        // #######################################################
        // #######################################################

        // public const string TRUE = "true";
        // public const string FALSE = "false";
        //
        // public const string STRING = "string";
        // public const string INT = "int";
        // public const string FLOAT = "float";
        // public const string BOOLEAN = "boolean";
        //
        // public const string READ_LINE = "readLine";
        // public const string READ_INTEGER = "readInteger";
        // public const string READ_FLOAT = "readFloat";
        // public const string PRINT = "print";
        // public const string PRINT_LINE = "printLine";
        // public const string EXIT = "exit";
        //
        // public const string TO_STRING = "toString";
        // public const string TO_INT = "toInt";
        // public const string TO_FLOAT = "toFloat";
        //
        // public const string IF = "if";
        // public const string ELSE = "else";
        // public const string FOR = "for";
        // public const string WHILE = "while";


        public static readonly string[] TYPES_ARRAY = {INT, FLOAT, STRING, BOOLEAN};
        public static readonly string[] PROCEDURES_ARRAY = {PRINT, PRINT_LINE, EXIT};

        public static readonly string[] FUNCTIONS_ARRAY =
            {TO_STRING, TO_INT, TO_FLOAT, READ_LINE, READ_INTEGER, READ_FLOAT};

        public static readonly string[] BLOCKS_ARRAY = {IF, ELSE, FOR, WHILE};
    }
}